// console.log("Hello")
//Scenario: Lets create a simple program that will perform CRUD operation for posting the available movies to watch.

// Mock database
let posts = [];
// Post ID
let count = 1;

//Add post data.
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
document.querySelector("#form-add-post").addEventListener("submit", (e) =>{

	// Prevent the page from loading
	e.preventDefault();


	posts.push({
		// "id" property will be used for the unique identification of each posts.
		id: count,
		//"title" and "body" values will come from the "form-add-post" element
		title:document.querySelector("#txt-title").value,
		body:document.querySelector("#txt-body").value
	});
	// count will increment everytime a new movie is added.
	count++;

	console.log(posts);
	alert("Successfully added!");
	showPosts();
})

const showPosts = () =>{
	//Create a variable that will contain all the posts
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post) =>{
		//onclick() is an event occurs when the user clicks on an element.
		// This allows us to execute a JavaScript's function when an element get clicked.
		// We can assign HTML elements in a JS variable.
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	//To check what is stored in the postEntries variables
	console.log(postEntries);

	//To replace the content of the "div-post-entries"
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}
// Edit Post Button
// We will create a function that will be called in the onclick() event and will pass the value in the Update Form input box.
const editPost = (id) =>{
	// Contain the value of the title and body in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body of the movie post to be updated in the Edit Post/Form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

// Update post.
// This will trigger an event that will update a post upon clicking the Update button.
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let postId = document.querySelector("#txt-edit-id").value
	let title = document.querySelector("#txt-edit-title").value
	let body = document.querySelector("#txt-edit-body").value

	console.log(postId);

	// Use a loop that will check each post stored in our mock database.
	for(i = 0; i < posts.length; i++){
		//Use a if statement to look for the post to be updated using id property.
		// The value posts[i].id is a Number while postId is a String, if we want to use the strictly equality operator (===) we need to convert the postId first.
		if(posts[i].id == postId){
			// reassign the value of the title and body property.
			posts[i].title = title;
			posts[i].body = body;

			// Invoke the showPosts() to check the updated posts.
			showPosts();

			alert("Successfully updated!");

			break;
		}
	}
})

//Activity Delete a post

/*
Instructions for s43 Activity:
Using the code so far, write the necessary code to delete a post.
Create the function (deletePost(id)) invoked on the “onclick” event of the delete button.
Make sure that the Now Showing List will be updated upon clicking the delete button.
Once done create a remote link and push to git with the commit message of WDC028-43.
Add the link in Boodle.
*/

const deletePost = (id) =>{

	for(i = 0; i < posts.length; i++){
		
		if(posts[i].id == id){

			posts.splice(i,1);
			showPosts();

			break;
		}
	}
}